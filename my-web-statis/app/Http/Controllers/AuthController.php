<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $fullName = ucwords(strtolower($firstName)." ".strtolower($lastName));

        return view('welcome',['fullName' => $fullName]);
    }
}
